class Mensagem {
  
  constructor(){
    this.id = 1;
    this.arrayMensagem = [];
  }

  enviar() {
    let mensagem = this.lerDados();

    if(this.validaCampos(mensagem) == true) {
      this.adicionar(mensagem)
    }

    this.listaTabela();
    this.limpar();
  }

  adicionar(mensagem) {
    this.arrayMensagem.push(mensagem);
    this.id++
  }

  listaTabela() {
    let tbody = document.querySelector('#tbody');
    tbody.innerText = "";

    for(let i = 0; i < this.arrayMensagem.length; i++) {
      let tr = tbody.insertRow();

      let td_id = tr.insertCell();
      let td_mensagem = tr.insertCell();
      
      td_id.innerText = this.arrayMensagem[i].id
      td_mensagem.innerText = this.arrayMensagem[i].texto
    }
  }

  lerDados() {
    let mensagem = {}

    mensagem.id = this.id;
    mensagem.texto = document.querySelector('#text_area').value;

    return mensagem
  }

  validaCampos(mensagem) {
    let alerta = "";
    if(mensagem.texto == ""){
      alerta += "Escreva algum caracter dentro da mensagem"
    }
    if(alerta != "") {
      alert(alerta);
      return false
    }

    return true
  }

  limpar() {
    document.querySelector('#text_area').value = "";
  }

  editar() { 
    let linha = prompt("Digite o número da linha que deseja editar: ")

    alert('Vamos editar a linha ' + linha)
  }

  excluir() {
    let tbody = document.querySelector('#tbody');

    let linha = prompt("Digite o número da linha que deseja excluir: ")

    for(let i = 0; i < this.arrayMensagem.length; i++){
      if(this.arrayMensagem[i].id == linha) {
        this.arrayMensagem.splice(i,1);
        tbody.deleteRow(i);
      }
    }
  }
}

var mensagem = new Mensagem();